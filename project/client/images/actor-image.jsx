
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ActorImage extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.svgId1 = `actor_image_id_${++ActorImage.ID}`;
    this.gradientId1 = `actor_image_id_${++ActorImage.ID}`;
    this.gradientId2 = `actor_image_id_${++ActorImage.ID}`;
    this.clipFaceId1 = `actor_image_id_${++ActorImage.ID}`;
    this.clipFaceId2 = `actor_image_id_${++ActorImage.ID}`;
    this.clipEyesId1 = `actor_image_id_${++ActorImage.ID}`;
    this.clipEyesId2 = `actor_image_id_${++ActorImage.ID}`;
    this.clipEyesId3 = `actor_image_id_${++ActorImage.ID}`;
    this.clipEyesId4 = `actor_image_id_${++ActorImage.ID}`;
    this.clipMouthId1 = `actor_image_id_${++ActorImage.ID}`;
    this.clipMouthId2 = `actor_image_id_${++ActorImage.ID}`;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
  }
  
  renderHappyEyes() {
    return (
      <>
        <defs>
          <clipPath id={this.clipEyesId1}>
            <rect x="-17.963" y="24.643" width="35.509" height="13.947" style={{fill:'#00ffff',opacity:.88,strokeWidth:.5}}/>
          </clipPath>
          <clipPath id={this.clipEyesId2}>
            <path d="m-187.33 23.75h40.461v25.231h-40.461zm40.937 18.69a20.621 10.097 0 0 0-20.621-10.097 20.621 10.097 0 0 0-20.621 10.097 20.621 10.097 0 0 0 20.621 10.097 20.621 10.097 0 0 0 20.621-10.097z" style={{fill:'#000000',strokeWidth:.5}}/>
          </clipPath>
          <mask id={this.clipEyesId4} maskUnits="userSpaceOnUse">
            <rect x="-187.94" y="17.834" width="100.11" height="18.311" style={{fill:'#00ffff',strokeWidth:.39097}}/>
          </mask>
          <clipPath id={this.clipEyesId3}>
            <rect x="43.387" y="24.643" width="35.509" height="13.947" style={{fill:'#00ffff',opacity:.88,strokeWidth:.5}}/>
          </clipPath>
        </defs>
        <g transform="translate(-48.095 -72.04)">
          <g transform="matrix(.26458 0 0 .26458 58.163 73.501)" clipPath={`url(#${this.clipEyesId3})`} style={{fill:'#000000'}}>
            <g style={{fill:'#000000'}}>
              <path transform="translate(227.26 2.3958)" d="m-151.87 36.365c-4.5191 10.664-25.942 10.664-30.461 0 3.0147-9.9982 27.128-10.829 30.461 0z" clipPath={`url(#${this.clipEyesId2})`} style={{fill:'#000000',strokeWidth:.5}}/>
           </g>
          </g>
          <g transform="matrix(.26458 0 0 .26458 58.163 73.501)" clipPath={`url(#${this.clipEyesId1})`} style={{fill:'#000000'}}>
            <path transform="translate(168.22 2.3964)" d="m-151.87 36.365c-4.5191 10.664-25.942 10.664-30.461 0 3.0147-9.9982 27.128-10.829 30.461 0z" clipPath={`url(#${this.clipEyesId2})`} mask={`url(#${this.clipEyesId4})`} style={{fill:'#000000',strokeWidth:.5}}/>
            <g style={{fill:'#000000'}}>
              <g style={{fill:'#000000'}}>
                <path transform="translate(168.22 2.3964)" d="m-151.87 36.365c-4.5191 10.664-25.942 10.664-30.461 0 3.0147-9.9982 27.128-10.829 30.461 0z" clipPath={`url(#${this.clipEyesId2})`} style={{fill:'#000000',strokeWidth:.5}}/>
              </g>
            </g>
          </g>
        </g>
      </>
    );
  }
  
  renderHappyMouth() {
    return (
      <>
        <defs>
          <clipPath id={this.clipMouthId1}>
            <rect x="157.19" y="81.681" width="50.312" height="15.863" style={{fill:'#00ffff',opacity:.81,strokeWidth:.32444}}/>
          </clipPath>
          <clipPath id={this.clipMouthId2}>
            <ellipse cx="166.94" cy="92.752" rx="44.665" ry="18.482" d="M 211.60769,92.752457 A 44.664925,18.482038 0 0 1 166.94276,111.2345 44.664925,18.482038 0 0 1 122.27784,92.752457 44.664925,18.482038 0 0 1 166.94276,74.270418 44.664925,18.482038 0 0 1 211.60769,92.752457 Z" style={{display:'none',fill:'#00ffff',strokeWidth:.55464}}/>
            <path d="m122.07 69.27h89.747v52.611h-89.747zm89.538 23.482a44.665 18.482 0 0 0-44.665-18.482 44.665 18.482 0 0 0-44.665 18.482 44.665 18.482 0 0 0 44.665 18.482 44.665 18.482 0 0 0 44.665-18.482z" style={{fill:'#00ffff',strokeWidth:.55464}}/>
          </clipPath>
        </defs>
        <g transform="translate(-48.095 -72.04)">
          <g transform="matrix(.26458 0 0 .26458 58.163 73.501)" style={{fill:'#000000'}}>
            <g transform="matrix(1.2299 0 0 1.2299 -193.91 -18.281)" clipPath={`url(#${this.clipMouthId1})`} style={{fill:'#000000'}}>
              <path transform="matrix(.65233 0 0 1 73.612 -20.193)" d="m206.82 95.576a39.873 21.306 0 0 1-39.873 21.306 39.873 21.306 0 0 1-39.873-21.306 39.873 21.306 0 0 1 39.873-21.306 39.873 21.306 0 0 1 39.873 21.306z" clipPath={`url(#${this.clipMouthId2})`} style={{fill:'#000000',strokeWidth:.6466}}/>
            </g>
          </g>
        </g>
      </>
    );
  }
  
  renderOrigDef(gradient, className) {
    const style = gradient ? {stopColor:'#ffffff'} : null;
    return (gradient ? (
      <linearGradient id={this.gradientId1} x1="85.044" x2="48.234" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
        <stop style={{stopColor:'#ffffff'}} offset="0"/>
        <stop className={className} offset="0.7"/>
      </linearGradient>
    ) : (
      <linearGradient id={this.gradientId1} x1="85.044" x2="48.234" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
        <stop className={className} offset="0"/>
        <stop className={className} offset="0.7"/>
      </linearGradient>
    ));
  }  
  
  renderOrigFace(className, gradient) {
    return (
      <>
        <defs>
          {this.renderOrigDef(gradient, className)}
        </defs>
        <g transform="translate(-48.095 -72.04)">
          <path d="m51.343 77.901c-0.46729 6.4835-0.49861 12.41 1.8452 18.243 2.0701 4.8317 6.6449 9.519 10.048 10.639 3.1257 0.93514 6.5325 0.51298 9.4631-1.3636 2.8381-1.8766 5.675-4.7414 7.5944-9.6711 1.7519-4.2279 1.851-12.866 1.5023-18.494-0.10415-1.7519-0.92328-3.8275-3.0785-3.0805s-7.341 1.9927-11.151 2.0221c-4.2614 0.20295-8.4599-0.77321-12.475-2.1147-1.7444-0.5435-3.3795-0.07766-3.5576 1.8434-0.04341 0.2718-0.02793-0.14819-0.19029 1.9758z" fill="#ffffff" stroke="#000000" strokeWidth="1.292" style={{fill:`url(#${this.gradientId1})`,stroke:'#000000'}}/>
        </g>
      </>
    );
  }
  
  renderTermFace(className) {
    return (
      <>
        <defs>
          <linearGradient id={this.gradientId1} x1="48.234" x2="85.044" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
            <stop style={{stopColor:'#ffffff'}} offset="0"/>
            <stop className={className} offset="0.7"/>
          </linearGradient>
        </defs>
        <g transform="translate(-48.095 -72.04)">
          <path d="m51.343 77.901c-0.46729 6.4835-0.49861 12.41 1.8452 18.243 2.0701 4.8317 6.6449 9.519 10.048 10.639 3.1257 0.93514 6.5325 0.51298 9.4631-1.3636 2.8381-1.8766 5.675-4.7414 7.5944-9.6711 1.7519-4.2279 1.851-12.866 1.5023-18.494-0.10415-1.7519-0.92328-3.8275-3.0785-3.0805s-7.341 1.9927-11.151 2.0221c-4.2614 0.20295-8.4599-0.77321-12.475-2.1147-1.7444-0.5435-3.3795-0.07766-3.5576 1.8434-0.04341 0.2718-0.02793-0.14819-0.19029 1.9758z" fill="#ffffff" stroke="#000000" strokeWidth="1.292" style={{fill:`url(#${this.gradientId1})`,stroke:'#000000'}}/>
        </g>
      </>
    );
  }
  
  renderProxyDef(part, gradient, className) {
    let left = null;
    let right = null;
    let leftGrad = null;
    let rightGrad = null;
    const style = gradient ? {stopColor:'#ffffff'} : null;
    if(ActorImage.PROXY_PART_ALL === part || ActorImage.PROXY_PART_LEFT === part) {
      left = (
        <clipPath id={this.clipFaceId2}>
          <rect x="48.234" y="72.133" width="18.7" height="37.042"/>
        </clipPath>
      );
      leftGrad = (gradient ? (
        <linearGradient id={this.gradientId2} x1="48.234" x2="85.044" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
          <stop style={style} offset="0"/>
          <stop className={className} offset="0.6"/>
        </linearGradient>
      ) : (
        <linearGradient id={this.gradientId2} x1="48.234" x2="85.044" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
          <stop className={className} offset="0"/>
          <stop className={className} offset="0.6"/>
        </linearGradient>
      ));
    }
    if(ActorImage.PROXY_PART_ALL === part|| ActorImage.PROXY_PART_RIGHT === part) {
      right = (
        <clipPath id={this.clipFaceId1}>
          <rect x="66.755" y="72.133" width="18.7" height="37.042"/>
        </clipPath>
      );
      rightGrad = (gradient ? (
        <linearGradient id={this.gradientId1} x1="85.044" x2="48.234" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
          <stop style={style} offset="0"/>
          <stop className={className} offset="0.6"/>
        </linearGradient>
      ) : (
        <linearGradient id={this.gradientId1} x1="85.044" x2="48.234" y1="72.133" y2="109.13" gradientUnits="userSpaceOnUse">
          <stop className={className} offset="0"/>
          <stop className={className} offset="0.6"/>
        </linearGradient>
      ));
    }
    return (
      <>
        {left}
        {leftGrad}
        {right}
        {rightGrad}
      </>
    );
  }
  
  renderProxyFace(className, part, grad) {
    return (
      <>
        <defs>
          {this.renderProxyDef(part, grad, className)}
        </defs>
        <g transform="translate(-48.095 -72.04)">
          <path d="m51.343 77.901c-0.46729 6.4835-0.49861 12.41 1.8452 18.243 2.0701 4.8317 6.6449 9.519 10.048 10.639 3.1257 0.93514 6.5325 0.51298 9.4631-1.3636 2.8381-1.8766 5.675-4.7414 7.5944-9.6711 1.7519-4.2279 1.851-12.866 1.5023-18.494-0.10415-1.7519-0.92328-3.8275-3.0785-3.0805s-7.341 1.9927-11.151 2.0221c-4.2614 0.20295-8.4599-0.77321-12.475-2.1147-1.7444-0.5435-3.3795-0.07766-3.5576 1.8434-0.04341 0.2718-0.02793-0.14819-0.19029 1.9758z" fill="#ffffff" stroke="#000000" strokeWidth="1.292" style={{fill:`url(#${this.gradientId1})`,stroke:'#000000'}} clipPath={`url(#${this.clipFaceId1})`} />
          <path d="m51.343 77.901c-0.46729 6.4835-0.49861 12.41 1.8452 18.243 2.0701 4.8317 6.6449 9.519 10.048 10.639 3.1257 0.93514 6.5325 0.51298 9.4631-1.3636 2.8381-1.8766 5.675-4.7414 7.5944-9.6711 1.7519-4.2279 1.851-12.866 1.5023-18.494-0.10415-1.7519-0.92328-3.8275-3.0785-3.0805s-7.341 1.9927-11.151 2.0221c-4.2614 0.20295-8.4599-0.77321-12.475-2.1147-1.7444-0.5435-3.3795-0.07766-3.5576 1.8434-0.04341 0.2718-0.02793-0.14819-0.19029 1.9758z" fill="#ffffff" stroke="#000000" strokeWidth="1.292" style={{fill:`url(#${this.gradientId2})`,stroke:'#000000'}} clipPath={`url(#${this.clipFaceId2})`} />
        </g>
      </>
    );
  }
  
  renderFace(className) {
    if('orig' === this.props.template.properties.face) {
      return this.renderOrigFace(className, true);
    }
    else if('term' === this.props.template.properties.face) {
      return this.renderTermFace(className);
    }
    else if('proxy' === this.props.template.properties.face) {
      return this.renderProxyFace(className, ActorImage.PROXY_PART_ALL, true);
    }
    else if('cond' === this.props.template.properties.face) {
      return this.renderOrigFace(className, true);
    }
    else if('local' === this.props.template.properties.face) {
      return this.renderOrigFace(className, false);
    }
    else if('sut' === this.props.template.properties.face) {
      return this.renderProxyFace(className, ActorImage.PROXY_PART_ALL, false);
    }
    else if('proxy_left' === this.props.template.properties.face) {
      return this.renderProxyFace(className, ActorImage.PROXY_PART_LEFT, true);
    }
    else if('proxy_right' === this.props.template.properties.face) {
      return this.renderProxyFace(className, ActorImage.PROXY_PART_RIGHT, true);
    }
  }
  
  renderEyes() {
    if('happy' === this.props.template.properties.eyes) {
      return this.renderHappyEyes();
    }
    else if('sad' === this.props.template.properties.eyes) {
      return this.renderSadEyes();
    }
  }
  
  renderMouth() {
    if('happy' === this.props.template.properties.mouth) {
      return this.renderHappyMouth();
    }
    else if('sad' === this.props.template.properties.mouth) {
      return this.renderSadMouth();
    }
  }
  
  render() {
    const className = `seq_dia_protocol_${this.props.stackName}`;
    return (
      <div className="modal_dialog_icon">
        <svg id={this.svgId1} width="100%" height="100%" version="1.1" viewBox="0 0 37.042 37.042" xmlns="http://www.w3.org/2000/svg">
          {this.renderFace(className)}
          {this.renderEyes()}
          {this.renderMouth()}
        </svg>
      </div>
    );
  }
}

ActorImage.ID = 0;

ActorImage.PROXY_PART_ALL = 0;
ActorImage.PROXY_PART_LEFT = 1;
ActorImage.PROXY_PART_RIGHT = 2;
