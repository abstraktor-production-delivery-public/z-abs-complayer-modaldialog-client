
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MsgImage extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
  }
  
  render() {
    return (
      <div className="modal_dialog_icon">
        <svg id={`${this.props.id}_svg`} width="100%" height="100%" viewBox="0 0 136.52 87.519" xmlns="http://www.w3.org/2000/svg">
          <g id={`${this.props.id}_g`} transform="translate(-32.924 -102.83)">
            <rect id={`${this.props.id}_rect`} x="35.924" y="105.83" width="130.52" height="81.519" ry="7.8849" style={{fill:'#fad137',strokeLinejoin:'round',strokeWidth:6,stroke:'#555'}}/>
            <path id={`${this.props.id}_path`} d="m38.787 108.69 62.356 46.738 62.578-46.435" style={{fill:'none',strokeLinejoin:'round',strokeWidth:6,stroke:'#555'}}/>
          </g>
        </svg>
      </div>
    );
  }
}


