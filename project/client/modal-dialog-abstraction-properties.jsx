
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import InputAnalyzer from './helper/input-analyzer';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogAbstractionProperties extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      name: '',
      description: '',
      valid: false,
      error: false,
      errorText: ''
    });
    this.originalName = '';
    this.originalDescription = '';
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.inputAnalyzer = new InputAnalyzer(this.props.capitalFirst ? true : false);
    this.hasEventListener = false;
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.props.onLoad && this.props.onLoad();
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.nameplaceholder, nextProps.nameplaceholder)
      || !this.shallowCompare(this.props.descriptionplaceholder, nextProps.descriptionplaceholder)
      || !this.shallowCompare(this.props.specialName, nextProps.specialName)
      || !this.shallowCompare(this.state, nextState);
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  show() {
    this.originalName = this.props.name ? this.props.name : '';
    this.originalDescription = this.props.description ? this.props.description : '';
    this.updateState({show: {$set: true}});
    this.updateState({name: {$set: this.originalName}});
    this.updateState({description: {$set: this.originalDescription}});
    this.updateState({valid: {$set: true}});
    this.updateState({error: {$set: false}});
    this.updateState({errorText: {$set: ''}});
    this.eventListener.init();
  }
  
  hide() {
    this.eventListener.exit();
    this.updateState(
      {show: {$set: false}
    });
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.add();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.clear();
      this.hide();
    }
  }
  
  add() {
    this.props.onAdd && this.props.onAdd(this.state.name, this.state.description.trim());
  }
  
  clear() {
    this.props.onClear && this.props.onClear();
  }
  
  renderErrorMessage() {
    if(this.state.error) {
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{this.state.errorText}</p>
        </div>
      );
    }
    else if(this.props.result.code !== 'success') {
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{this.props.result.msg}</p>
        </div>
      );
    }
  }
  
  render() {
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    else if(this.state.error) {
      nameDivLabelClassName += ' has-error';
    }
    const inputNameClass = this.state.error ? 'form-control has-error' : 'form-control';
    const buttonSpecializedName = this.props.specialName ? this.props.specialName + '_' : '';
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id={`modal_abstraction_${buttonSpecializedName}properties`} show={this.state.show || errorShow}
        onShown={(e) => {
          this.inputRef.current.focus();
        }}
        onHide={(e) => {
          this.clear();
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <label htmlFor="modal_dialog_abstraction_properties_name" className="col-sm-2 control-label">Name</label>
            <div className={nameDivLabelClassName}>
              <input ref={this.inputRef} id="modal_dialog_abstraction_properties_name" type="text" className="form-control" value={this.state.name} placeholder={this.props.nameplaceholder}
                onChange={(e) => {
                  const result = this.inputAnalyzer.analyze(e.target.value);
                  if(result.success) {
                    this.updateState({valid: {$set: true}});
                    this.updateState({error: {$set: false}});
                  }
                  else {
                    this.updateState({valid: {$set: false}});
                    this.updateState({error: {$set: true}});
                    if(!result.upperCaseValidation) {
                      this.updateState({errorText: {$set: 'The name must start with a capital letter'}});
                    }
                    else if(!result.regexpValidation) {
                      this.updateState({errorText: {$set: `The name contains not allowed characters: '${result.noneValidCharacters}'`}});
                    }
                    else if(!result.filenameValidation) {
                      this.updateState({errorText: {$set: `The filename is not allowed: '${result.noneValidFilename}'`}});
                    }
                  }
                  this.updateState({name: {$set: result.text}});
                }}
                />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="modal_dialog_abstraction_properties_description" className="col-sm-2 control-label">Description</label>
            <div className="col-sm-10">
              <input type="text" id="modal_dialog_abstraction_properties_description" className="form-control" value={this.state.description} placeholder={this.props.descriptionplaceholder}
                onChange={(e) => {
                  this.updateState({description: {$set: e.target.value}});
                }}
                />
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!this.state.show}>
            <button id={`abstraction_properties_${buttonSpecializedName}properties_button`} type="button" className="btn btn-primary" disabled={!this.state.valid || (this.originalName === this.state.name && this.originalDescription === this.state.description)}
              onClick={(e) => {
                this.add();
                this.hide();
              }}
              >Change</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id={`abstraction_properties_${buttonSpecializedName}close_button`} type="button" className="btn btn-default" disabled={!this.state.show}
              onClick={(e) => {
                this.clear();
                this.hide();
             }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}

