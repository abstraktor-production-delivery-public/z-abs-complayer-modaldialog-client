
'use strict';


class InputAnalyzer {
  constructor(capitalFirst) {
    this.capitalFirst = capitalFirst;
    this.noneValidCharsRegexp = /[&/\\\?%*|"<>.,;= ]/g;
    this.noneValidFilenames = ['CON', 'PRN', 'AUX', 'CLOCK$', 'NUL', 'COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6', 'COM7', 'COM8', 'COM9', 'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5', 'LPT6', 'LPT7', 'LPT8', 'LPT9'];
  }
  
  analyze(text) {
    text = text.trim();
    const result = {
      success: true,
      text: text,
      upperCaseValidation: true,
      regexpValidation: true,
      filenameValidation: true,
      noneValidCharacters: '',
      noneValidFilename: ''
    };
    
    if(this.capitalFirst) {
      const first = text.charAt(0);
      result.upperCaseValidation = (undefined !== this.capitalFirst && !this.capitalFirst) ? true: (first !== first.toLowerCase() && first === first.toUpperCase());
    }
    this.noneValidCharsRegexp.lastIndex = 0;
    result.regexpValidation = !this.noneValidCharsRegexp.test(text);
    if(!result.regexpValidation) {
      for(let i = 0; i < text.length; ++i) {
        this.noneValidCharsRegexp.lastIndex = 0;
        if(this.noneValidCharsRegexp.test(text.charAt(i))) {
          result.noneValidCharacters += text.charAt(i);
        }
      }
    }
    result.filenameValidation = -1 === this.noneValidFilenames.indexOf(text);
    if(!result.filenameValidation) {
      result.noneValidFilename = text;
    }
    result.success = result.upperCaseValidation && result.regexpValidation && result.filenameValidation;
    return result;
  }
}

module.exports = InputAnalyzer;
