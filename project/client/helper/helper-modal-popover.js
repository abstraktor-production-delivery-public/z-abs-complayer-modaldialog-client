
'use strict';

import React from 'react';


class HelperModalPopover {
  constructor() {
    this.modalRef = React.createRef();
    this.popoverRefs = [];
    this.popoverId = -1;
  }
  
  didMount() {
    this.modalRef.current.setHideables(this.popoverRefs); 
  }
  
  onRender() {
    this.popoverId = -1;
  }
  
  getPopoverRef() {
    const id = ++this.popoverId;
    if(this.popoverRefs[id]) {
      return this.popoverRefs[id];
    }
    else {
      const popoverRef = React.createRef();
      this.popoverRefs.push(popoverRef);
      return popoverRef;
    }
  }
}

module.exports = HelperModalPopover;
