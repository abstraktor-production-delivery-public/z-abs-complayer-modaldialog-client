
'use strict';


class HelperEventListener {
  constructor(event, boundEvent, useCapture) {
    this.event = event;
    this.boundEvent = boundEvent;
    this.useCapture = useCapture;
    this.hasEventListener = false;
  }
  
  init() {
    if(!this.hasEventListener) {
      this.hasEventListener = true;
      window.addEventListener(this.event, this.boundEvent, this.useCapture);
    }
  }
  
  exit() {
    if(this.hasEventListener) {
      this.hasEventListener = false;
      window.removeEventListener(this.event, this.boundEvent, this.useCapture);
    }
  }
}


module.exports = HelperEventListener;
