
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import InputAnalyzer from './helper/input-analyzer';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import MarkupDocumentationPage from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-page';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogAbstractionAdd extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      name: '',
      description: '',
      repo: '',
      wizardName: '',
      templateName: '',
      wizard: {
        viewIndex: -1,
        view: '',
        heading: '',
        testDataViews: [],
        optionalViews: new Map()
      },
      valid: false,
      error: false,
      errorText: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.inputAnalyzer = new InputAnalyzer(this.props.capitalFirst ? true : false);
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.props.onLoad && this.props.onLoad();
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.nameplaceholder, nextProps.nameplaceholder)
      || !this.shallowCompare(this.props.descriptionplaceholder, nextProps.descriptionplaceholder)
      || !this.shallowCompare(this.props.specialName, nextProps.specialName)
      || !this.shallowCompare(this.props.wizardNames, nextProps.wizardNames)
      || !this.shallowCompare(this.props.templates, nextProps.templates)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.state.wizardName !== prevState.wizardName || this.state.templateName !== prevState.templateName || this.props.templates !== prevProps.templates) {
      const template = this._getTemplate(true);
      if(template) {
        if('object' === typeof template) {
          if(template.testDataViews) {
            const testDataValueViews = [];
            const optionalViews = new Map();
            template.testDataViews.forEach((testDataTemplateView) => {
              if('single' === testDataTemplateView.view) {
                const testDataValueView = {
                  name: testDataTemplateView.name,
                  view: testDataTemplateView.view,
                  data: []
                };
                testDataValueViews.push(testDataValueView);
                testDataTemplateView.data.forEach((data) => {
                  testDataValueView.data.push({
                    name: data.name,
                    value: 'preview' !== data.type ? data.value : '',
                    include: 'optional:false' === data.include ? false : true,
                    preview: 'preview' === data.type,
                    valid: data.valid
                  });
                });
              }
              else if('optional' === testDataTemplateView.view) {
                const testDataValueOptionalViews = {
                  name: testDataTemplateView.name,
                  view: testDataTemplateView.view,
                  optionalName: testDataTemplateView.optionalName,
                  views: []
                };
                testDataValueViews.push(testDataValueOptionalViews);
                if(0 !== testDataTemplateView.views.length) {
                  optionalViews.set(testDataTemplateView.optionalName, {
                    name: testDataTemplateView.views[0].name,
                    index: 0
                  });
                }
                testDataTemplateView.views.forEach((view) => {
                  const testDataValueViews = {
                    name: view.name,
                    data: []
                  };
                  testDataValueOptionalViews.views.push(testDataValueViews);
                  testDataValueViews.name = view.name;
                  view.data.forEach((data) => {
                    testDataValueViews.data.push({
                      name: data.name,
                      value: 'preview' !== data.type ? data.value : '',
                      include: 'optional:false' === data.include ? false : true,
                      preview: 'preview' === data.type,
                      valid: data.valid
                    });
                  });
                });
              }
              else if('plugin' === testDataTemplateView.view) {
                ddb.error('plugin not implemented');
              }
            });
            if('single' === template.testDataViews[0].view
               || 'optional' === template.testDataViews[0].view
               || 'plugin' === template.testDataViews[0].view) {
              this.updateState({wizard: {
                viewIndex: {$set: 0},
                view: {$set: template.testDataViews[0].view},
                heading: {$set: template.testDataViews[0].name},
                testDataViews: {$set: testDataValueViews},
                optionalViews: {$set: optionalViews}
              }});
            }
            else {
              this.updateState({wizard: {
                viewIndex: {$set: -1},
                view: {$set: ''},
                heading: {$set: ''},
                testDataViews: {$set: []}
              }});
            }
          }
        }
        else if('string' === typeof template) {
          this.updateState({templateName: {$set: template}});
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  show() {
    this.updateState({show: {$set: true}});
    this.updateState({name: {$set: ''}});
    this.updateState({description: {$set: ''}});
    this.updateState({repo: {$set: 'actorjs-data-local'}});
    this.updateState({wizardName: {$set: this.props.wizardNames && 1 <= this.props.wizardNames.length ? this.props.wizardNames[0] : ''}});
    this.updateState({templateName: {$set: ''}});
    this.updateState({wizard: {
      viewIndex: {$set: -1},
      view: {$set: ''},
      heading: {$set: ''},
      testDataViews: {$set: []}
    }});
    this.updateState({valid: {$set: false}});
    this.updateState({error: {$set: false}});
    this.updateState({errorText: {$set: ''}});
    this.eventListener.init();
  }
  
  hide() {
    this.eventListener.exit();
    this.updateState({
      show: {$set: false},
      valid: {$set: false}
    });
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error && this.state.valid) {
        e.preventDefault();
        this.add();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.clear();
      this.hide();
    }
  }
  
  add() {
    if('tc' === this.props.type) {
      this.props.onAdd && this.props.onAdd(this.state.name, this.state.description, this.state.wizardName, this.state.templateName, this.state.wizard.testDataViews);
    }
    else if('ts' === this.props.type || 'fut' === this.props.type || 'repo' === this.props.type) {
      this.props.onAdd && this.props.onAdd(this.state.name, this.state.description);
    }
    else if('sut' === this.props.type) {
      this.props.onAdd && this.props.onAdd(this.state.name, this.state.description, this.state.repo);
    }
  }
  
  clear() {
    this.props.onClear && this.props.onClear();
  }
  
  renderErrorMessage() {
    if(this.state.error) {
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{this.state.errorText}</p>
        </div>
      );
    }
    else if(this.props.result.code !== 'success') {
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      };
      const labelStyle = {
        top: '6px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{this.props.result.msg}</p>
        </div>
      );
    }
  }
  
  renderRepoOptions() {
    const repos = [...this.props.repos];
    repos.sort((a, b) => {
      if('actorjs-data-local' === a || 'actorjs-data-global' === b) {
        return -1;
      }
      else if('actorjs-data-local' === b || 'actorjs-data-global' === a) {
        return 1;
      }
      else if(a < b) {
        return -1;
      }
      else if(a > b) {
        return 1;
      }
      else {
        return 0;
      }
    });
    return repos.map((repo, index) => {
      return (
        <option key={index} value={repo}>{repo}</option>
      );
    });
  }
  
  renderRepo() {
    if(this.props.repos) {
      return (
        <>
          <br />
          <div className="row" style={{textAlign: 'right'}}>
            <div className={ModalDialogAbstractionAdd.COLUMN_LIST_START}>
              <label htmlFor="modal_dialog_abstraction_add_repo" className="control-label">Repo</label>
            </div>
            <div className={ModalDialogAbstractionAdd.COLUMN_LIST_END}>
              <select className="form-control input-sm" id="modal_dialog_abstraction_add_repo" value={this.state.repo}
                onChange={(e) => {
                  this.updateState({repo: {$set: e.target.value}});
                }}
              >
                {this.renderRepoOptions()}
              </select>
            </div>
          </div>
        </>
      );
    }
  }
  
  renderDescription() {
    const id = 'modal_dialog_abstraction_add_description' + (this.props.id ? '_' + this.props.id : '');
    return (
      <>
        <br />
        <div className="row" style={{textAlign: 'right'}}>
          <div className={ModalDialogAbstractionAdd.COLUMN_LIST_START}>
            <label htmlFor={id} className="control-label">Description</label>
          </div>
          <div className={ModalDialogAbstractionAdd.COLUMN_LIST_END}>
            <input type="text" id={id} className="form-control input-sm" value={this.state.description} placeholder={this.props.descriptionplaceholder}
              onChange={(e) => {
                this.updateState({description: {$set: e.target.value}});
              }}
            />
          </div>
        </div>
      </>
    );
  }
  
  renderWizardOptions() {
    return this.props.wizardNames.map((wizardName, index) => {
      return (
        <option key={index} value={wizardName}>{wizardName}</option>
      );
    });
  }
  
  renderWizard() {
    if(this.props.wizardNames && 0 !== this.props.wizardNames.length) {
      return (
        <>
          <br />
          <div className="row" style={{textAlign: 'right'}}>
            <div className={ModalDialogAbstractionAdd.COLUMN_LIST_START}>
              <label htmlFor="modal_dialog_abstraction_add_wizard" className="control-label">Wizard</label>
            </div>
            <div className={ModalDialogAbstractionAdd.COLUMN_LIST_END}>
              <select id="modal_dialog_abstraction_add_wizard" className="form-control input-sm" value={this.state.wizardName}
                onChange={(e) => {
                  const wizardName = e.target.value;
                  this.updateState({wizardName: {$set: wizardName}});
                  this.updateState({templateName: {$set: ''}});
                  this.updateState({wizard: {
                    viewIndex: {$set: -1},
                    view: {$set: ''},
                    heading: {$set: ''},
                    testDataViews: {$set: []}
                  }});
                }}
              >
                {this.renderWizardOptions()}
              </select>
            </div>
          </div>
        </>
      );
    }
  }
  
  renderTemplateOptions() {
    const templates = this.props.templates.get(this.state.wizardName);
    const options = [];
    let index = -1;
    templates.forEach((template, displayName) => {
      options.push(
        <option key={++index} value={displayName}>{displayName}</option>
      );
    });
    return options;
  }
  
  renderTemplate(template) {
    if(this.props.wizardNames && 0 !== this.props.wizardNames.length && 'none' !== this.state.wizardName) {
      if(template) {
        return (
          <>
          <br />
            <div className="row" style={{textAlign: 'right'}}>
              <div className={ModalDialogAbstractionAdd.COLUMN_LIST_START}>
                <label htmlFor="modal_dialog_abstraction_add_template" className="control-label">Template</label>
              </div>
              <div className={ModalDialogAbstractionAdd.COLUMN_LIST_END}>
                <select id="modal_dialog_abstraction_add_template" className="form-control input-sm" value={template.displayName}
                  onChange={(e) => {
                    const templateName = e.target.value;
                    this.updateState({templateName: {$set: templateName}});
                    this.updateState({wizard: {
                      viewIndex: {$set: -1},
                      view: {$set: ''},
                      heading: {$set: ''},
                      testDataViews: {$set: []}
                    }});
                  }}
                >
                  {this.renderTemplateOptions()}
                </select>
              </div>
            </div>
          </>
        );
      }
    }
  }
  
  renderMarkup(template) {
    const document = MarkupDocumentationPage.parse(template ? template.markup : (this.props.defaultMarkup ? this.props.defaultMarkup.markup : ''));
    const style = {marginTop:'-16px'};
    if(!template && this.props.defaultMarkup) {
      Object.assign(style, this.props.defaultMarkup.markupStyle);
    }
    return (
      <div style={style}>
        <ComponentDocument document={document} waitMount></ComponentDocument>
      </div>
    );
  }
  
  renderTestDataHeading(heading) {
    return (
      <>
        <div className="modal_abstraction_add_test_data_heading">
          <div className={`modal_abstraction_add_test_data_heading_inner ${ModalDialogAbstractionAdd.COLUMN_TEST_DATA_START}`}>
            <p className="control-label modal_body_p_as_label">{heading}</p>
          </div>
        </div>
      </>
    );
  }
  
  _getPreviewValue(name, prefixes, testDataValueView) {
    const foundValue = testDataValueView.data.find((data) => {
      return data.name === name;
    });
    if(foundValue && foundValue.include) {
      if(prefixes) {
        const foundPrefix = prefixes.find((prefix) => {
          return prefix[0] === name;
        });
        if(foundPrefix) {
          return foundPrefix[1] + foundValue.value;
        }
      }
      return foundValue.value;
    }
    else {
      return '';
    }
  }
  
  _setPreviewValue(name, testDataValueView, value, valid) {
    const foundValue = testDataValueView.data.find((data) => {
      return data.name === name;
    });
    if(foundValue) {
      foundValue.value = value;
      foundValue.valid = valid;
    }
  }
  
  _formatPreview(previewFormat, prefixes, testDataValueView) {
    const regexp = /\${([^}]*)}/g;
    const formatters = [...previewFormat.matchAll(regexp)];
    let preview = previewFormat;
    formatters.forEach((formatter) => {
      const value = this._getPreviewValue(formatter[1], prefixes, testDataValueView);
      preview = preview.replace(formatter[0], value);
    }); 
    return preview;
  }
  
  _validate(testDataTemplate, name, preview) {
    if(!testDataTemplate.validate) {
      return 0; // NONE
    }
    else {
      try {
        return new Function('name', 'preview', testDataTemplate.validate)(name, preview);
      }
      catch(err) {
        ddb.error(err);
        return 3; // ERROR
      }
    }
  }
  
  _getValidDivClass(valid) {
    switch(valid) {
      case 0:
        return '';
      case 1:
        return 'has-success';
      case 2:
        return 'has-warning';
      case 3:
        return 'has-error';
    }
  }
  
  _getValidInputClass(valid) {
    switch(valid) {
      case 0:
        return '';
      case 1:
        return 'alert alert-success';
      case 2:
        return 'alert alert-warning';
      case 3:
        return 'alert alert-danger';
    }
  }
  
  _updatePreview(testDataTemplateView, testDataValueView) {
    testDataValueView.data.forEach((testDataValue) => {
      if(testDataValue.preview) {
        const foundTestDataTemplate = testDataTemplateView.data.find((testDataTemplate) => {
          return testDataTemplate.name === testDataValue.name;
        });
        if(foundTestDataTemplate) {
          const preview = this._formatPreview(foundTestDataTemplate.value, foundTestDataTemplate.prefixes, testDataValueView);
          const valid = this._validate(foundTestDataTemplate, testDataValue.name, preview);
          this._setPreviewValue(testDataValue.name, testDataValueView, preview, valid);
        }
      }
    });
  }
  
  renderInputTextarea(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    const foundTestDataValue = testDataValueView.data.find((testDataValue) => {
      return testDataTemplate.name === testDataValue.name;
    });
    const absoluteStyleInner = {
      position: 'relative',
      marginBottom: '8px'
    };
    const textAreaStyle = {
      position: 'relative',
      top: '2px',
      left: '6px',
      overflow: 'auto',
      resize: 'none',
      display: 'block',
      margin: '0px',
      padding: '4px',
      fontFamily: 'Menlo, Monaco, Consolas, \"Courier New\", monospace',
      fontSize: '100%',
      width: 'calc(100% - 12px)',
      lineHeight: 1.09,
      height: '100%'
    };
    return (
      <div key={`value_${index}`} style={absoluteStyleInner}>
        <textarea id={this.props.id} className="form-control" style={textAreaStyle} value={testDataValue.value} rows={testDataTemplate.rows} wrap="off"
          onChange={(e) => {
            const value = e.target.value;
            testDataValue.value = value;
            this._updatePreview(testDataTemplateView, testDataValueView);
            this.updateState({wizard: {testDataViews: {$set: this.state.wizard.testDataViews}}});
          }}
        />
      </div>
    );
  }
  
  renderInputText(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    const foundTestDataValue = testDataValueView.data.find((testDataValue) => {
      return testDataTemplate.name === testDataValue.name;
    });
    const idCheckbox = 'modal_dialog_abstraction_input_checkbox' + (this.props.id ? '_' + this.props.id : '');
    const idText = 'modal_dialog_abstraction_input_text' + (this.props.id ? '_' + this.props.id : '');
    return (
      <div className="col_sm_8">
        <div className={`input-group ${this._getValidDivClass(foundTestDataValue.valid)}`} style={{marginRight:'4px'}}>
          <span className="input-group-addon">
            <input type="checkbox" id={idCheckbox} defaultChecked={testDataValue.include} disabled={!testDataTemplate.include || !testDataTemplate.include.startsWith('optional:')} aria-label="..." onChange={(e) => {
                const checked = e.target.checked;
                testDataValue.include = checked;
                this._updatePreview(testDataTemplateView, testDataValueView);
                this.updateState({wizard: {testDataViews: {$set: this.state.wizard.testDataViews}}});
              }}
            />
          </span>
          <input type="text" id={idText} className={`form-control input-sm ${this._getValidInputClass(foundTestDataValue.valid)}`} id={`modal_dialog_abstraction_add_input_text_${index}`} value={testDataValue.value} placeholder={testDataTemplate.placeHolder} disabled={!testDataValue.include}
            onChange={(e) => {
              const value = e.target.value.trim();
              const foundTestDataTemplate = testDataTemplateView.data.find((testDataTemplate) => {
                return testDataTemplate.name === testDataValue.name;
              });
              const valid = this._validate(foundTestDataTemplate, testDataValue.name, value);
              testDataValue.value = value;
              testDataValue.valid = valid;
              this._updatePreview(testDataTemplateView, testDataValueView);
              this.updateState({wizard: {testDataViews: {$set: this.state.wizard.testDataViews}}});
            }}
          />
        </div>
      </div>
    );
  }
  
  renderInputRadio(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    const radios = testDataTemplate.values.map((value, i) => {
      const radioId = `modal_dialog_abstraction_add_input_radio_${index}_${i}`;
      const currentIndex = i;
      return (
        <div key={i} className="col_sm_4 input_column" style={{textAlign:'left',paddingTop:'3px',width:'auto'}}>
          <input type="radio" id={radioId} style={{position:'relative',top:'1px'}} aria-label="..." checked={testDataTemplate.values[currentIndex] === testDataValue.value} onChange={(e) => {
            testDataValue.value = testDataTemplate.values[currentIndex];
            this._updatePreview(testDataTemplateView, testDataValueView);
            this.updateState({wizard: {testDataViews: {$set: this.state.wizard.testDataViews}}});
          }}/>
          <label htmlFor={radioId} style={{paddingLeft:'4px'}}>{value}</label>
        </div>
      );
    });
    return (
      <div className="col_sm_8">
        {radios}
      </div>
    );
  }
  
  renderInputCheckbox(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    const checkboxId = `modal_dialog_abstraction_add_input_checkbox_${index}`;
    return (
      <div className="col_sm_8">
        <div className="col_sm_4 input_column" style={{textAlign:'left',paddingTop:'5px',width:'auto'}}>
          <input type="checkbox" id={checkboxId} style={{position:'relative',top:'-1px'}} aria-label="..." checked={testDataValue.value} onChange={(e) => {
            testDataValue.value = e.target.checked;
            this._updatePreview(testDataTemplateView, testDataValueView);
            this.updateState({wizard: {testDataViews: {$set: this.state.wizard.testDataViews}}});
          }}/>
        </div>
      </div>
    );
  }
  
  renderPreview(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    const foundTestDataValue = testDataValueView.data.find((testDataValue) => {
      return testDataTemplate.name === testDataValue.name;
    });
    const preview = foundTestDataValue ? foundTestDataValue.value : '';
    return (
      <div className="col_sm_8">
        <div key={index} className={`input_column ${this._getValidDivClass(foundTestDataValue.valid)}`} style={{textAlign:'left',paddingLeft:'0px'}}>
          <input type="text" className={`form-control input-sm ${this._getValidInputClass(foundTestDataValue.valid)}`} value={preview} readOnly style={{paddingLeft:'4px',marginBottom:'0px',fontWeight:'bold'}} />
        </div>
      </div>
    );
  }
  
  renderInputName(input, testDataTemplate, index) {
    return (
      <div key={`value_${index}`} className="row test_data_row" style={{textAlign: 'right'}}>
        <div className={ModalDialogAbstractionAdd.COLUMN_TEST_DATA_START} style={{right:'-20px'}}>
          <label htmlFor={`modal_dialog_abstraction_add_description_${index}`} className="control-label test_data_name">{testDataTemplate.name}</label>
        </div>
        {input}
      </div>
    );
  }
  
  renderInput(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index) {
    if(testDataTemplate.depends) {
      const params = testDataTemplate.depends.split(',');
      const dependName = params[0];
      const dependValues = params[1].split('|');
      const found = testDataValueView.data.find((dataValue) => {
        return dependName === dataValue.name;
      });
      if(!found || !dependValues.includes(found.value)) {
        return null;
      }
    }
    if('input' === testDataTemplate.type) {
      const input = this.renderInputText(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
      return this.renderInputName(input, testDataTemplate, index);
    }
    else if('radio' === testDataTemplate.type) {
      const input = this.renderInputRadio(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
      return this.renderInputName(input, testDataTemplate, index);
    }
    else if('checkbox' === testDataTemplate.type) {
      const input = this.renderInputCheckbox(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
      return this.renderInputName(input, testDataTemplate, index);
    }
    else if('preview' === testDataTemplate.type) {
      const input = this.renderPreview(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
      return this.renderInputName(input, testDataTemplate, index);
    }
    else if('textarea' === testDataTemplate.type) {
      return this.renderInputTextarea(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
    }
    else {
      return null;
    }
  }
  
  renderTestDataViewSingle(testDataTemplateView, testDataValueView) {
    const testDataInput = [];
    testDataTemplateView.data.forEach((testDataTemplate, index) => {
      if(undefined !== testDataTemplate.heading) {
        testDataInput.push(
          <div key={`header_${index}`} className="row test_data_row">
            {this.renderTestDataHeading(testDataTemplate.heading)}
          </div>
        );
      }
      const currentIndex = index;
      const testDataValue = testDataValueView.data[currentIndex];
      const input = this.renderInput(testDataTemplate, testDataTemplateView, testDataValue, testDataValueView, index);
      testDataInput.push(input);
    });
    return testDataInput;
  }
  
  renderTestDataViewOptional(testDataTemplateView) {
    const testDataValueOptionalView = this.state.wizard.testDataViews[this.state.wizard.viewIndex];
    const optionalViews = testDataTemplateView.views.map((testCaseDataView, index) => {
      const currentIndex = index;
      const radioId = `radio_${testCaseDataView.name}`;
      return (
        <div key={currentIndex} className="modal_abstraction_add_abstraction_optional">
          <input type="radio" id={radioId} checked={this.state.wizard.optionalViews.get(testDataValueOptionalView.optionalName)?.name === testDataValueOptionalView.views[currentIndex].name} aria-label="..." style={{top:'2px',position:'relative'}} disabled={testCaseDataView.disabled} onChange={(e) => {
            this.updateState({wizard: {optionalViews: (optionalViews) => {
              //optionalViews.set(testDataValueOptionalView.optionalName, testDataValueOptionalView.views[currentIndex].name);
              const optionalView = optionalViews.get(testDataValueOptionalView.optionalName);
              optionalView.name = testDataValueOptionalView.views[currentIndex].name;
              optionalView.index = currentIndex;
            }}});
          }}/>
          <label htmlFor={radioId} style={{paddingLeft:'4px',position:'relative',top:'2px'}}>{testCaseDataView.name}</label>
        </div>
      );
    });
    const optionalView = this.state.wizard.optionalViews.get(testDataValueOptionalView.optionalName);
    return (
      <>
        <div className="input-group" style={{marginTop:'-6px',marginBottom:'6px',display:'inline-table'}}>
          <span className="input-group-addon" style={{paddingTop:'8px'}}>
            {optionalViews}
          </span>
        </div>
        {this.renderTestDataViewSingle(testDataTemplateView.views[optionalView.index], testDataValueOptionalView.views[optionalView.index])}
      </>
    );
  }
  
  renderTestDataViewPlugin(testDataTemplateView) {
    const groups = testDataTemplateView.groups.map((group, index) => {
      return (
        <div key={index} style={{backgroundColor:'blue'}}>
          {group.name}
        </div>
      );
    });
    return (
      <div style={{backgroundColor:'red'}}>
        {groups}
      </div>
    );
  }
  
  renderTestDataView(testDataTemplateView) {
    const view = this.state.wizard.view;

    if('single' === view) {
      return this.renderTestDataViewSingle(testDataTemplateView, this.state.wizard.testDataViews[this.state.wizard.viewIndex]);
    }
    else if('optional' === view) {
      return this.renderTestDataViewOptional(testDataTemplateView);
    }
    else if('plugin' === view) {
      return this.renderTestDataViewPlugin(testDataTemplateView);
    }
  }
  
  renderTestDataWizardNavigationButtons(step, steps, template) {
    if(2 <= steps) {
      const buttonSpecializedName = this.props.specialName ? this.props.specialName + '_' : '';
      return (
        <>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id={`abstraction_add_${buttonSpecializedName}previous_button`} type="button" className="btn btn-primary modal_abstraction_add_test_case_data" disabled={1 === step}
              onClick={(e) => {
                this.updateState({wizard: {
                  viewIndex: {$set: this.state.wizard.viewIndex - 1},
                  view: {$set: template.testDataViews[this.state.wizard.viewIndex - 1].view},
                  heading: {$set: template.testDataViews[this.state.wizard.viewIndex - 1].name}
                }});
              }}
            >Previous</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id={`abstraction_add_${buttonSpecializedName}next_button`} type="button" className="btn btn-primary modal_abstraction_add_test_case_data" disabled={steps === step}
              onClick={(e) => {
                this.updateState({wizard: {
                  viewIndex: {$set: this.state.wizard.viewIndex + 1},
                  view: {$set: template.testDataViews[this.state.wizard.viewIndex + 1].view},
                  heading: {$set: template.testDataViews[this.state.wizard.viewIndex + 1].name}
                }});
              }}
            >Next</button>
          </Popover>
        </>
      );
    }
    else {
      return null;
    }
  }
  
  renderWizardView() {
    
  }
  
  renderRegressionText(regressionFriendly) {
    let text = '';
    if('yes' === regressionFriendly) {
      text = 'Use in regression';
    }
    else if('no' === regressionFriendly) {
      text = 'Do not use in regression';
    }
    else if('not_optimal' === regressionFriendly) {
      text = 'Not optimal to use in regression';
    }
    else {
      return null;
    }
    return (
      <>
        <p className={`modal_abstraction_add_test_case_data_regression modal_abstraction_add_test_case_data_regression_flag_${regressionFriendly}`}>&#9873;&nbsp;</p>
        <p className="modal_abstraction_add_test_case_data_regression">{text}</p>
      </>
    );
  }
  
  renderTestDataWizard(template) {
    if(-1 !== this.state.wizard.viewIndex) {
      const testDataTemplateView = template.testDataViews[this.state.wizard.viewIndex];
      if(testDataTemplateView.name === this.state.wizard.heading) {
        const view = this.state.wizard.testDataViews[this.state.wizard.viewIndex];
        const step = this.state.wizard.viewIndex + 1;
        const steps = this.state.wizard.testDataViews.length;
        return (
          <div className="modal_abstraction_add_test_case_data">
            <div className="modal_abstraction_add_test_case_data_caption">
              <p className="modal_abstraction_add_test_case_data_display">
                {testDataTemplateView.displayName}
              </p>
              {this.renderRegressionText(template.regressionFriendly)}
            </div>
            <div className="modal_abstraction_add_test_case_data_step">
              {this.renderTestDataView(testDataTemplateView)}
            </div>
            <div className="modal_abstraction_add_test_case_data_footer">
              <div className="modal_abstraction_add_test_case_step">
                {`${(step)}/${steps}`}
              </div>
              {this.renderTestDataWizardNavigationButtons(step, steps, template)}
            </div>
          </div>
        );
      }
    }
    return null;
  }
  
  render() {
    const template = this._getTemplate();
    const markupNodes = template ? template.markupNodes : 2;
    const infoStyle = {
      width: 3 === markupNodes ? '308px' : '178px',
      height: this.state.wizardName ? (template ? '180px' : '130px') : (this.props.repos ? '130px' : '80px'),
      position: 'absolute',
      right: '16px',
      borderRadius: '3px',
      border: 'solid 1.5px lightgray',
      backgroundColor: 'AliceBlue'
    };
    const width = 3 === markupNodes ? '730px' : undefined;
    let nameDivLabelClassName = ModalDialogAbstractionAdd.COLUMN_LIST_END;
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    else if(this.state.error) {
      nameDivLabelClassName += ' has-error';
    }
    const inputNameClass = this.state.error ? 'form-control has-error' : 'form-control';
    const buttonSpecializedName = this.props.specialName ? this.props.specialName + '_' : '';
    this.helperModalPopover.onRender();
    const idAddName = 'modal_dialog_abstraction_add_name' + (this.props.id ? '_' + this.props.id : '');
    return (
      <Modal ref={this.helperModalPopover.modalRef} id={`modal_abstraction_${buttonSpecializedName}add`} show={this.state.show || errorShow} width={width}
        onShown={(e) => {
          this.inputRef.current.focus();
        }}
        onHide={(e) => {
          this.clear();
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div style={infoStyle}>
            {this.renderMarkup(template)}
          </div>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className={ModalDialogAbstractionAdd.COLUMN_LIST_START}>
                <label htmlFor={idAddName} className="control-label">Name</label>
              </div>
              <div className={nameDivLabelClassName}>
                <input ref={this.inputRef} id={idAddName} type="text" className="form-control input-sm" value={this.state.name} placeholder={this.props.nameplaceholder}
                  onChange={(e) => {
                    const result = this.inputAnalyzer.analyze(e.target.value);
                    if(result.success) {
                      this.updateState({valid: {$set: true}});
                      this.updateState({error: {$set: false}});
                    }
                    else {
                      this.updateState({valid: {$set: false}});
                      this.updateState({error: {$set: true}});
                      if(!result.upperCaseValidation) {
                        this.updateState({errorText: {$set: 'The name must start with a capital letter'}});
                      }
                      else if(!result.regexpValidation) {
                        this.updateState({errorText: {$set: `The filename contains not allowed characters: '${result.noneValidCharacters}'`}});
                      }
                      else if(!result.filenameValidation) {
                        this.updateState({errorText: {$set: `The filename is not allowed: '${result.noneValidFilename}'`}});
                      }
                    }
                    this.updateState({name: {$set: result.text}});
                  }}
                  />
              </div>
            </div>
            {this.renderDescription()}
            {this.renderWizard()}
            {this.renderTemplate(template)}
            {this.renderTestDataWizard(template)}
            {this.renderRepo()}
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!this.state.show}>
            <button id={`abstraction_add_${buttonSpecializedName}add_button`} type="button" className="btn btn-primary" disabled={!this.state.valid}
              onClick={(e) => {
                this.add();
                this.hide();
              }}
              >Add</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id={`abstraction_add_${buttonSpecializedName}close_button`} type="button" className="btn btn-default" disabled={!this.state.show}
              onClick={(e) => {
                this.clear();
                this.hide();
             }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
  
  _getTemplate(getFirstName=false) {
    if(this.state.wizardName && this.props.templates) {
      const templates = this.props.templates.get(this.state.wizardName);
      if(templates && 0 !== templates.size) {
        if(this.state.templateName) {
    		  return templates.get(this.state.templateName);
        }
        else if(getFirstName) {
          const [firstTemplateName] = templates.keys();
          return firstTemplateName;
        }
      }
    }
    return null;
  }
}


ModalDialogAbstractionAdd.COLUMN_LIST_START = 'col_sm_2';
ModalDialogAbstractionAdd.COLUMN_TEST_DATA_START = 'col_sm_4';
ModalDialogAbstractionAdd.COLUMN_LIST_END = 'col_sm_6';
