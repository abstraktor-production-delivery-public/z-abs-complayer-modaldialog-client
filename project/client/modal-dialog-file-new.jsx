
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import InputAnalyzer from './helper/input-analyzer';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import MarkupDocumentationPage from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-page';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileNew extends ReactComponentBase {
  constructor(props) {
    super(props, {
      fileName: '',
      fileType: '',
      template: null,
      dynamicTemplateName: '',
      dynamicTemplateNames: [],
      dynamicTemplateHeading: '',
      dynamicTemplate2Index: -1,
      dynamicTemplate2Names: [],
      dynamicTemplate2MarkupNodes: [],
      dynamicTemplate2MarkupStyles: [],
      dynamicTemplate2Markups: [],
      dynamicTemplate2Heading: '',
      valid: false,
      error: false,
      errorText: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.inputAnalyzer = new InputAnalyzer(this.props.capitalFirst ? true : false);
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  static getDerivedStateFromProps(props, state) {
    let fileType = null;
    if(props.folder) {
      for(let i = 0; i < props.folder.data.types.length; ++i) {
        if('actorjs' !== props.folder.data.types[i]) {
          fileType = props.folder.data.types[i];
          break;
        }
      }
    }
    const template = state.template ? state.template : (props?.templateNames && props.templateNames.length >= 1) ? props.templateNames[0] : '';
    let dynamicTemplateNames = [];
    let dynamicTemplateHeading = '';
    if(props.onTemplate) {
      const dynamicTemplate = props.onTemplate(template.name);
      if(dynamicTemplate) {
        dynamicTemplateNames = dynamicTemplate.names;
        dynamicTemplateHeading = dynamicTemplate.heading;
      }
    }
    
    return {
      fileType: fileType,
      template: template,
      dynamicTemplateNames: dynamicTemplateNames,
      dynamicTemplateHeading: dynamicTemplateHeading
    };
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.props.templateNames, nextProps.templateNames)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  hide() {
    this.updateState({
      fileName: {$set: ''},
      fileType: {$set: ''},
      template: {$set: null},
      dynamicTemplateName: {$set: ''},
      dynamicTemplateNames: {$set: []},
      dynamicTemplateHeading: {$set: ''},
      dynamicTemplate2Index: {$set: -1},
      dynamicTemplate2Names: {$set: []},
      dynamicTemplate2MarkupNodes: {$set: []},
      dynamicTemplate2MarkupStyles: {$set: []},
      dynamicTemplate2Markups: {$set: []},
      dynamicTemplate2Heading: {$set: ''},
      valid: {$set: false},
      error: {$set: false},
      errorText: {$set: ''}
    });
    this.props.onHide();
  }
  
  new() {
    this.props.onFileNew(this.props.folder.projectId, `${this.props.folder.data.path}/${this.props.folder.title}`, this.state.fileName, this.state.template.type, this.state.template.name, this.state.dynamicTemplateName, -1 !== this.state.dynamicTemplate2Index ? this.state.dynamicTemplate2Names[this.state.dynamicTemplate2Index] : null);
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.new();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="file_new_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  rendertemplateNameOptions() {
    if(this.props.templateNames) {
      return this.props.templateNames.map((template, index) => {
        return (
          <option key={index} value={template.name}>
            {template.name}
          </option>
        );
      });
    }
  }
  
  renderIcon() {
    if(this.props.onRenderIcon) {
      return (
        <>
          {this.props.onRenderIcon(this.state.template, this.state.dynamicTemplateName)}
        </>
      );
    }
  }
  
  renderTemplates() {
    return (
      <>
        <br />
        <div className="row" style={{textAlign: 'right'}}>
          <div className={ModalDialogFileNew.COLUMN_LIST_START}>
            <label htmlFor="modal_dialog_file_new_template_name" className="control-label">Type</label>
          </div>
          <div className={this.state.template.icon ? ModalDialogFileNew.COLUMN_LIST_WITH_ICON : ModalDialogFileNew.COLUMN_LIST_END}>
            <select id="modal_dialog_file_new_template_name" className="form-control input-sm" value={this.state.template.name}
              onChange={(e) => {
                const template = this._getTemplate(e.target.value);
                if(this.props.onTemplate) {
                  const dynamicTemplate = this.props.onTemplate(template.name);
                  if(dynamicTemplate) {
                    this.updateState({dynamicTemplateNames: {$set: dynamicTemplate.names}, dynamicTemplateHeading: {$set: dynamicTemplate.heading}});
                  }
                  else {
                    this.updateState({dynamicTemplateNames: {$set: []}, dynamicTemplateHeading: {$set: ''}});
                  }
                }
                this.updateState({template: {$set: template}});
                this.updateState({dynamicTemplateName: {$set: ''}});
                this.updateState({dynamicTemplate2Index: {$set: -1}});
                this.updateState({dynamicTemplate2Names: {$set: []}});
                this.updateState({dynamicTemplate2MarkupNodes: {$set: []}});
                this.updateState({dynamicTemplate2MarkupStyles: {$set: []}});
                this.updateState({dynamicTemplate2Markups: {$set: []}});
                this.updateState({dynamicTemplate2Heading: {$set: ''}});
              }}
            >
              {this.rendertemplateNameOptions()}
            </select>
          </div>
          {this.renderIcon()}
        </div>
      </>
    );
  }
  
  renderdynamicTemplateNameOptions() {
    return this.state.dynamicTemplateNames.map((dynamicTemplateName, index) => {
      return (
        <option key={index} value={dynamicTemplateName}>{dynamicTemplateName}</option>
      );
    });
  }
  
  renderDynamicTemplates() {
    if(0 !== this.state.dynamicTemplateNames.length) {
      return (
        <>
          <br />
          <div className="row" style={{textAlign: 'right'}}>
            <div className={ModalDialogFileNew.COLUMN_LIST_START}>
              <label htmlFor="modal_dialog_file_new_stack_template_name" className="control-label">{this.state.dynamicTemplateHeading}</label>
            </div>
            <div className={ModalDialogFileNew.COLUMN_LIST_END}>
              <select id="modal_dialog_file_new_stack_template_name" className="form-control input-sm" value={this.state.dynamicTemplateName}
                onChange={(e) => {
                  const dynamicTemplateName = e.target.value;
                  if(this.props.onDynamicTemplate) {
                    const dynamicTemplate2 = this.props.onDynamicTemplate(this.state.template.name, dynamicTemplateName);
                    if(dynamicTemplate2) {
                      this.updateState({
                        dynamicTemplate2Names: {$set: dynamicTemplate2.names},
                        dynamicTemplate2Index: {$set: 0},
                        dynamicTemplate2MarkupNodes: {$set: dynamicTemplate2.markupNodes},
                        dynamicTemplate2MarkupStyles: {$set: dynamicTemplate2.markupStyles},
                        dynamicTemplate2Markups: {$set: dynamicTemplate2.markups},
                        dynamicTemplate2Heading: {$set: dynamicTemplate2.heading}
                      });
                    }
                    else {
                      this.updateState({
                        dynamicTemplate2Names: {$set: []},
                        dynamicTemplate2Index: {$set: -1},
                        dynamicTemplate2MarkupNodes: {$set: []},
                        dynamicTemplate2MarkupStyles: {$set: []},
                        dynamicTemplate2Markups: {$set: []},
                        dynamicTemplate2Heading: {$set: ''}
                      });
                    }
                  }
                  this.updateState({dynamicTemplateName: {$set: e.target.value}});
                }}
              >
                {this.renderdynamicTemplateNameOptions()}
              </select>
            </div>        
          </div>
        </>
      );
    }
  }
  
  renderdynamicTemplateNameOptions2() {
    return this.state.dynamicTemplate2Names.map((dynamicTemplateName2, index) => {
      return (
        <option key={index} value={dynamicTemplateName2}>{dynamicTemplateName2}</option>
      );
    });
  }
  
  renderDynamicTemplates2() {
    if(0 !== this.state.dynamicTemplate2Names.length) {
      return (
        <>
          <br />
          <div className="row" style={{textAlign: 'right'}}>
            <div className={ModalDialogFileNew.COLUMN_LIST_START}>
              <label htmlFor="modal_dialog_file_new_stack_template_name" className="control-label">{this.state.dynamicTemplate2Heading}</label>
            </div>
            <div className={ModalDialogFileNew.COLUMN_LIST_END}>
              <select id="modal_dialog_file_new_stack_template_name" className="form-control input-sm" value={this.state.dynamicTemplate2Names[this.state.dynamicTemplate2Index]}
                onChange={(e) => {
                  const index = this.state.dynamicTemplate2Names.indexOf(e.target.value);
                  this.updateState({dynamicTemplate2Index: {$set: index}});
                }}
              >
                {this.renderdynamicTemplateNameOptions2()}
              </select>
            </div>        
          </div>
        </>
      );
    }
  }
  
  renderMarkup() {
    if(0 !== this.state.dynamicTemplate2Names.length) {
      if(-1 !== this.state.dynamicTemplate2Index) {
        const document = MarkupDocumentationPage.parse(this.state.dynamicTemplate2Markups[this.state.dynamicTemplate2Index]);
        const style = {marginTop:'-16px'};
        const dynamicStyle = this.state.dynamicTemplate2MarkupStyles[this.state.dynamicTemplate2Index];
        if(dynamicStyle) {
          Object.assign(style, dynamicStyle);
        }
        return (
          <div style={style}>
            <ComponentDocument document={document} waitMount></ComponentDocument>
          </div>
        );
      }
    }
    else {
      const document = MarkupDocumentationPage.parse(this.state.template.markup);
      const style = {marginTop:'-16px'};
      Object.assign(style, this.state.template.markupStyle);
      return (
        <div style={style}>
          <ComponentDocument document={document}></ComponentDocument>
        </div>
      );
    }
  }
  
  renderFileTypeOptions() {
    if(null !== this.props.folder) {
      return this.props.folder.data.types.map((type, index) => {
        return (
          <option key={index} value={type}>{type}</option>
        );
      });
    }
  }
  
  renderFileType() {
    if(this.props.fileType) {
      return (
        <>
          <br />
          <div className="row" style={{textAlign: 'right'}}>
            <div className={ModalDialogFileNew.COLUMN_LIST_START}>
              <label htmlFor="modal_dialog_file_new_file_type" className="control-label">File Type</label>
            </div>
            <div className={ModalDialogFileNew.COLUMN_LIST_END}>
              <select id="modal_dialog_file_new_file_type" className="form-control input-sm" value={this.state.fileType}
                onChange={(e) => {
                  this.updateState({fileType: {$set: e.target.value}});
                }}
              >
                {this.renderFileTypeOptions()}
              </select>
            </div>
          </div>
        </>
      )
    }
  }
  
  renderInner(show) {
    if(show) {
      return (
        <>
          {this.renderTemplates()}
          {this.renderDynamicTemplates()}
          {this.renderDynamicTemplates2()}
          {this.renderFileType()}
        </>
      );
    }
    else {
      return null;
    }
  }
  
  render() {
    const modalResult = this.props.modalResults.get(this.props.name);
    const markupNodes = 0 === this.state.dynamicTemplate2Names.length ? 2 : this.state.dynamicTemplate2MarkupNodes[this.state.dynamicTemplate2Index];
    const infoStyle = {
      width: 3 === markupNodes ? '308px' : '178px',
      height: 0 === this.state.dynamicTemplateNames.length ? (this.props.fileType ? '130px' : '80px') : (0 === this.state.dynamicTemplate2Names.length ? (this.props.fileType ? '180px' : '130px') : (this.props.fileType ? '230px': '180px')),
      position: 'absolute',
      right: '16px',
      borderRadius: '3px',
      border: 'solid 1.5px lightgray',
      overflow: 'hidden',
      backgroundColor: 'AliceBlue'
    };
    const width = 3 === markupNodes ? '730px' : undefined;
    const show = !!modalResult?.visible;
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_file_new" aria-labelledby="contained-modal-title-sm" show={show}
        onShown={(e) => {
          this.inputRef.current.focus();
        }}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div style={infoStyle}>
            {this.renderMarkup()}
          </div>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className={ModalDialogFileNew.COLUMN_LIST_START}>
                <label htmlFor="modal_dialog_file_new_name" className="control-label">Name</label>
              </div>
              <div className={ModalDialogFileNew.COLUMN_LIST_END}>
                <input ref={this.inputRef} id="modal_dialog_file_new_name" type="text" className="form-control input-sm" placeholder="file name" value={this.state.fileName}
                  onChange={(e) => {
                    const result = this.inputAnalyzer.analyze(e.target.value);
                    if(result.success) {
                      this.updateState({valid: {$set: true}});
                      this.updateState({error: {$set: false}});
                    }
                    else {
                      this.updateState({valid: {$set: false}});
                      this.updateState({error: {$set: true}});
                      if(!result.regexpValidation) {
                        this.updateState({errorText: {$set: `The name contains not allowed characters: '${result.noneValidCharacters}'`}});
                      }
                      else if(!result.filenameValidation) {
                        this.updateState({errorText: {$set: `The filename is not allowed: '${result.noneValidFilename}'`}});
                      }
                    }
                    this.updateState({fileName: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
            {this.renderInner(show)}
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!show}>
            <button id="file_new_new_button" type="button" className="btn btn-primary" disabled={0 === this.state.fileName.length || !this.state.valid}
              onClick={(e) => {
                this.new();
             }}
            >New</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="file_new_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
  
  _getTemplate(name) {
    return this.props.templateNames.find((template) => {
      return template.name === name;
    });
  }
}


ModalDialogFileNew.COLUMN_LIST_START = 'col_sm_2';
ModalDialogFileNew.COLUMN_LIST_WITH_ICON = 'col_sm_5_5';
ModalDialogFileNew.COLUMN_LIST_END = 'col_sm_6';
