
'use strict';

import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class ModalDialogStore extends StoreBaseRealtime {
  constructor() {
    super({
      modalResults: new Map()
    });
    this.pendingName = '';
  }
  
  onActionModalDialogShow(action) {
    this.updateState({modalResults: (modalResults) => {
      const modalResultNew = {
        name: action.name,
        visible: true,
        result: {
          code: 'success',
          error: null,
          msg: ''
        }
      };
      modalResults.set(action.name, modalResultNew);
    }});
  }
  
  onActionModalDialogHide(action) {
    this.updateState({modalResults: (modalResults) => {
      const modalResultNew = {
        name: action.name,
        visible: false,
        result: {
          code: 'success',
          error: null,
          msg: ''
        }
      };
      modalResults.set(action.name, modalResultNew);
    }});
  }
  
  onActionModalDialogPending(action) {
    this.pendingName = action.name;
  }
  
  onActionModalDialogResult(action) {
    this.updateState({modalResults: (modalResults) => {
      const modalResultNew = {
        name: this.pendingName,
        visible: 'error' === action.result.code,
        result: action.result
      };
      modalResults.set(this.pendingName, modalResultNew);
    }});
    this.pendingName = '';
  }
}


module.exports = new ModalDialogStore();
