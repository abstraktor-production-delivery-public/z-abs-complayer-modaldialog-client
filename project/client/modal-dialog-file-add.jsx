
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import FileIcons from 'z-abs-complayer-bootstrap-client/client/icons/file-icons';
import FolderIcons from 'z-abs-complayer-bootstrap-client/client/icons/folder-icons';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import Tree from 'z-abs-complayer-tree-client/client/react-components/tree';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileAdd extends ReactComponentBase {
  constructor(props) {
    super(props, {
      name: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.name, nextProps.name)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.folder, nextProps.folder)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.props.project, nextProps.project)
      || !this.shallowCompare(this.props.current, nextProps.current)
      || !this.shallowCompare(this.props.original, nextProps.original)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  hide() {
    this.updateState({name: {$set: ''}});
    this.props.onHide();
  }
  
  _keyDown(e) {
    const disabled = (null === this.props.current.file) || !this.props.current.file.data.path.startsWith(`${this.props.original.folder.data.path}/${this.props.original.folder.title}`);
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!disabled) {
        e.preventDefault();
        this.props.onAdd(this.props.folder.projectId, this.props.current.file.title, this.props.current.file.data.path, this.props.current.file.data.type);
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
    else if('ArrowUp' === e.key) {
      
    }
    else if('ArrowDown' === e.key) {
      
    }
    else if('ArrowLeft' === e.key) {
      
    }
    
    else if('ArrowRight' === e.key) {
      
    }
  }
  
  setName(name) {
    this.updateState({name: {$set: name}});    
  }
  
  setDescription(description) {
    this.updateState({
      description: {$set: description}
    });    
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="file_add_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }

  renderTree() {
    const activationFilter = null !== this.props.original.folder ? `${this.props.original.folder.data.path}/${this.props.original.folder.title}` : undefined;
    return (
      <Tree id="actor_tree_modul_dialog_file" project={this.props.project} current={this.props.current} expandCurrentFolder={true} allowExpand={false} activationFilter={activationFilter} folderIcons={FolderIcons} fileIcons={FileIcons}
        onClickFile={(key, title, path, type) => {
          this.props.project.select(key, true);
          this.props.onChoose(`${path}/${title}`);
        }}
        onClickFolder={(key, title, path, types) => {}}
        onToggleFolder={(key, expanded) => {}}
      />
    );
  }
  
  render() {
    this.helperModalPopover.onRender();
    const modalResult = this.props.modalResults.get(this.props.name);
    const show = !!modalResult?.visible;
    const disabled = (null === this.props.current.file) || !this.props.current.file.data.path.startsWith(`${this.props.original.folder.data.path}/${this.props.original.folder.title}`);
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_file_add" show={show}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          {this.renderTree()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!show}>
            <button id="file_add_add_button" type="button" className="btn btn-primary" disabled={disabled}
              onClick={(e) => {
                this.props.onAdd(this.props.folder.projectId, this.props.current.file.title, this.props.current.file.data.path, this.props.current.file.data.type);
                this.hide();
              }}
            >Add</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="file_add_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}

