
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionModalDialogShow extends Action {
  constructor(name) {
    super(name);
  }
}

export class ActionModalDialogHide extends Action {
  constructor(name) {
    super(name);
  }
}

export class ActionModalDialogPending extends Action {
  constructor(name) {
    super(name);
  }
}

export class ActionModalDialogResult extends Action {
  constructor(name, result) {
    super(name, result);
  }
}
