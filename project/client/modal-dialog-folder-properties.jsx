
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import InputAnalyzer from './helper/input-analyzer';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFolderProperties extends ReactComponentBase {
  constructor(props) {
    super(props, {
      folder: null,
      folderName: '',
      types: [],
      valid: false,
      error: false,
      errorText: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.inputAnalyzer = new InputAnalyzer(this.props.capitalFirst ? true : false);
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  static getDerivedStateFromProps(props, state) {
    if(0 === state.types.length) {
      const types = props.folder?.data.types.map((type) => {
        return {
          chosen: true,
          name: type
        }
      });
      return {
        types: types ? types : []
      };
    }
    else {
      return null;
    }
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.name, nextProps.name)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.folder, nextProps.folder)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  hide() {
    this.updateState({
      folder: {$set: null},
      folderName: {$set: ''},
      types: {$set: []},
      valid: {$set: false},
      error: {$set: false},
      errorText: {$set: ''}
    });
    this.props.onHide();
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.update();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide()
      ;
    }
  }
  
  renderTypes() {
    if(null !== this.props.folder) {
      return this.props.folder.data.types.map((type, index) => {
        if('actorjs' !== type) {
          return (
            <div key={index} className="row">
              <div className="col-sm-2" style={{textAlign: 'right'}}>
                <label htmlFor={`folder_properties_input_type${type}`} className="control-label">{type}</label>
              </div>
              <div className="col-sm-2">
                <input type="checkbox" id={`folder_properties_input_type${type}`} aria-label="..." autoComplete="off" checked={this.state.types[index]?.chosen} style={{marginTop: '12px'}}
                  onChange={(e) => {
                    this.updateState({types: (types) => {
                      types[index].chosen = e.currentTarget.checked;
                    }});
                  }}
                />
              </div>
            </div>
          );
        }
      });
    }
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="folder_properties_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  render() {
    const modalResult = this.props.modalResults.get(this.props.name);
    const origTypes = this.props.folder?.data.types.map((type) => {
      return {
        chosen: true,
        name: type
      }
    });
    const show = modalResult?.visible;
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_folder_properties" aria-labelledby="contained-modal-title-sm" show={show}
        onShown={(e) => {
          this.inputRef.current.select();
        }}
        onHide={(e) => {
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-2">
                <label htmlFor="modal_dialog_folder_properties_name" className="control-label">Name</label>
              </div>
              <div className="col-sm-9">
                <input ref={this.inputRef} type="text" id="modal_dialog_folder_properties_name" className="form-control input-sm" placeholder="folder name" value={this.state.folderName ? this.state.folderName : this.props.folder ? this.props.folder.title : ''}
                  onChange={(e) => {
                    const result = this.inputAnalyzer.analyze(e.target.value);
                    if(result.success) {
                      this.updateState({valid: {$set: true}});
                      this.updateState({error: {$set: false}});
                    }
                    else {
                      this.updateState({valid: {$set: false}});
                      this.updateState({error: {$set: true}});
                      if(!result.regexpValidation) {
                        this.updateState({errorText: {$set: `The name contains not allowed characters: '${result.noneValidCharacters}'`}});
                      }
                      else if(!result.filenameValidation) {
                        this.updateState({errorText: {$set: `The folder is not allowed: '${result.noneValidFilename}'`}});
                      }
                    }
                    this.updateState({folderName: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
            {this.renderTypes()}
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!show}>
            <button id="folder_properties_change_button" type="button" className="btn btn-primary" disabled={(0 === this.state.folderName.length || this.props.folder.title === this.state.folderName)}
              onClick={(e) => {
                let types = this.state.types.map((type) => {
                  if(type.chosen) {
                    return type.name;
                  }
                });
                const data = this.deepCopy(this.props.folder.data);
                data.types = types;
                this.props.onFolderProperties(this.props.folder.projectId, this.props.folder.title, this.state.folderName, data);
              }}
            >Change</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="folder_properties_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}
