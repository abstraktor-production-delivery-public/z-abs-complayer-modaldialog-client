
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import FileIcons from 'z-abs-complayer-bootstrap-client/client/icons/file-icons';
import FolderIcons from 'z-abs-complayer-bootstrap-client/client/icons/folder-icons';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import Tree from 'z-abs-complayer-tree-client/client/react-components/tree';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import Project  from 'z-abs-corelayer-cs/clientServer/project';
import React from 'react';


export default class ModalDialogWorkspaceOpen extends ReactComponentBase {
  constructor(props) {
    super(props, {
      name: '',
      types: []
    });
    this.helperModalPopover = new HelperModalPopover();
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  static getDerivedStateFromProps(props, state) {
    let update = null;
    if(0 === state.types.length) {
      const types = props.folder?.data.types.map((type) => {
        return {
          chosen: 'actorjs' !== type,
          name: type
        }
      });
      update = {
        types: types ? types : []
      };
    }
    return update;
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.name, nextProps.name)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      //|| !this.shallowCompare(this.props.project, nextProps.project)
      //|| !this.shallowCompare(this.props.current, nextProps.current)
      //|| !this.shallowCompare(this.props.original, nextProps.original)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  hide() {
    this.updateState({
      name: {$set: ''},
      types: {$set: []}
    });
    this.props.onHide();
  }
  
  _keyDown(e) {
    const disabled = null === this.props.current.folder || !this.props.current.folder.data.path.startsWith(`${this.props.original.folder.data.path}/${this.props.original.folder.title}`);
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!disabled) {
        e.preventDefault();
        const types = [];
        this.state.types.forEach((type) => {
          if(type.chosen) {
            types.push(type.name);
          }
        });
        const data = this.deepCopy(this.props.current.folder.data);
        data.path = `${this.props.folder.data.path}/${this.props.folder.title}`;
        data.types = types;
        this.props.onAdd();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
    else if('ArrowUp' === e.key) {
      
    }
    else if('ArrowDown' === e.key) {
      
    }
    else if('ArrowLeft' === e.key) {
      
    }
    
    else if('ArrowRight' === e.key) {
      
    }
  }
  
  setName(name) {
    this.updateState({
      name: {$set: name}
    });    
  }
  
  setDescription(description) {
    this.updateState({
      description: description
    });    
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="worksapce_opn_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  renderTree() {
    if(0 !== this.props.workspaces.length) {
      const project = new Project();
      let node = project.addRootFolder('workspaces', [], 'key', true);
      this.props.workspaces.forEach((workspace) => {
        project.addFolderToNode(workspace, '.', [], node);
      });
      return (
        <Tree id="actor_tree_modal_dialogworkspace_open" project={project} current={{folder:null, file:null}} expandCurrentFolder={true} allowExpand={false} folderIcons={FolderIcons} fileIcons={FileIcons}
          onClickFile={(key, title, path, type) => {}}
          onClickFolder={(key, title, data) => {
            console.log('KLOCLSDASDASDASDASDASDASDASDASDASDASDASDA');
            project.select(key, true);
            this.props.onChoose(title);
          }}
          onToggleFolder={(key, expanded) => {}}
        />
      );
    }
    else {
      return null;
    }
  }
  
  /*renderFileTypes(disabled) {
    if(!disabled && this.props.folder) {
      const fileTypes = this.props.folder.data.types.map((type, index) => {
        if('actorjs' !== type) {
          return (
            <React.Fragment key={`worksapce_opn_input_${type}`}>
              <div className="col-sm-1">
                <input type="checkbox" id={`worksapce_opn_input_type_${type}`} aria-label="..." autoComplete="off" checked={this.state.types[index]?.chosen}
                  onChange={(e) => {
                    this.state.types[index].chosen = e.currentTarget.checked;
                    this.updateState({types: {$set: this.state.types}});
                  }}
                />
              </div>
              <div className="col-sm-1">
                <label htmlFor={`worksapce_opn_input_type_${type}`} className="modal_body_file_type control-label">{type}</label>
              </div>
            </React.Fragment>
          );
        }
      });
      return (
        <>
          <hr className="modal_body_hr" />
          <div className="row">
            <div className="col-sm-1" />
            <h5 className="modal_body_file_type_heading col-sm-4">File Types</h5>
          </div>
          <div key="file_types" className="row">
          <div className="col-sm-1" />
            {fileTypes}
          </div>
        </>
      );
    }
  }*/
  
  _calculateDepth(node, currentDepth, depth) {
    if(depth > currentDepth.value) {
      ++currentDepth.value;
    }
    node.children.forEach((child) => {
      if(child.folder) {
        this._calculateDepth(child, currentDepth, ++depth);
      }
    });
  }
  
  /*renderNotFound(disabled) {
    if(disabled && 0 !== this.props.project.source.length) {
       const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      const currentDepth = {value: 0};
      this._calculateDepth(this.props.project.source[0], currentDepth, 0);
      return (
        <div style={errorDivStyle}>
          <p id="worksapce_opn_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>No Folders Found</p>
        </div>
      );
    }
  }*/
  
  render() {
    this.helperModalPopover.onRender();
    const modalResult = this.props.modalResults.get(this.props.name);
    const show = !!modalResult?.visible;
   // const disabled = null === this.props.current.folder || !this.props.current.folder.data.path.startsWith(`${this.props.original.folder.data.path}/${this.props.original.folder.title}`);
    const disabled = false;
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_worksapce_opn" aria-labelledby="contained-modal-title-sm" show={show}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody className="modal-dialog-file-body">
          {this.renderTree()}
          {/*this.renderFileTypes(disabled)*/}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          {/*this.renderNotFound(disabled)*/}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!show}>
            <button id="worksapce_opn_add_button" type="button" className="btn btn-primary" disabled={disabled}
              onClick={(e) => {
                const types = [];
                this.state.types.forEach((type) => {
                  if(type.chosen) {
                    types.push(type.name);
                  }
                });
                {/*const data = this.deepCopy(this.props.current.folder.data);
                data.path = `${this.props.folder.data.path}/${this.props.folder.title}`;
                data.types = types;*/}
                this.props.onAdd();
              }}
            >Add</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="worksapce_opn_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}


module.exports = ModalDialogWorkspaceOpen;
