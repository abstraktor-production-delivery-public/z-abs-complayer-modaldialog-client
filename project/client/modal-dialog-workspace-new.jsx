
'use strict';

import HelperModalPopover from './helper/helper-modal-popover';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogWorkspaceNew extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      appName: '',
      appType: 0
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.helperModalPopover = new HelperModalPopover();
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show() {
    this.updateState({
      show: {$set: true},
      appName: {$set: ''},
      appType: {$set: 0}
    });
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  hide() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
    this.updateState({
      show: {$set: false},
      appName: {$set: ''},
      appType: {$set: 0}
    });
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.new();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
   /* if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label">{this.props.result.msg}</p>
        </div>
      );
    }*/
  }
  
  render() {
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_workspace_new" aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-3">
                <label htmlFor="workspace_new_app_name" className="control-label">App Name</label>
              </div>
              <div className="col-sm-8">
                <input type="text" id="workspace_new_app_name" className="form-control input-sm" placeholder="app name" value={this.state.appName}
                  onChange={(e) => {
                    this.updateState({appName: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
            <br />
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-3">
                <label htmlFor="workspace_new_workspace_name" className="control-label">App Type</label>
              </div>
              <div className="col-sm-8">
                <div className="col-sm-3">
                  <input type="radio" id="radio-app" style={{position:'relative',top:'1px'}} aria-label="..." checked={0 === this.state.appType} onChange={(e) => {
                    this.updateState({appType: {$set: 0}});
                  }}/>
                  <label htmlFor="radio-app" style={{paddingLeft:'4px'}}>{"App"}</label>
                </div>
                <div className="col-sm-3">
                  <input type="radio" id="radio-node" style={{position:'relative',top:'1px'}} aria-label="..." checked={1 === this.state.appType} onChange={(e) => {
                    this.updateState({appType: {$set: 1}});
                  }}/>
                  <label htmlFor="radio-node" style={{paddingLeft:'4px'}}>{"Node"}</label>
                </div>
              </div>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!this.state.show}>
            <button id="workspace_new_add_button" type="button" className="btn btn-primary" disabled={0 === this.state.appName.length}
              onClick={(e) => {
                this.props.onWorkspaceNew(this.state.appName, this.state.appType);
                this.hide();
              }}
              >Add</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id="workspace_new_close_button" type="button" className="btn btn-default" disabled={!this.state.show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}
