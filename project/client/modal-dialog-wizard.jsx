
'use strict';

import HelperModalPopover from './helper/helper-modal-popover';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogWizard extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      folder: null,
      name: ''
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.helperModalPopover = new HelperModalPopover();
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state, nextState);
  }
  
  show(folder) {
    this.updateState({
      show: {$set: true},
      folder: {$set: folder},
      name: {$set: ''}
    });
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  hide() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
    this.updateState({
      show: {$set: false},
      folder: {$set: null}
    });
  }  
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.new();
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
   /* if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label">{this.props.result.msg}</p>
        </div>
      );
    }*/
  }
  
  renderChildren() {
    return (
      <>
        <br />
        {this.props.children}
      </>
    );
  }
  
  render() {
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_wizard" aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onHide={(e) => {
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-2">
                <label htmlFor="modal_dialog_wizard_name" className="control-label">Name</label>
              </div>
              <div className="col-sm-9">
                <input type="text" id="modal_dialog_wizard_name" className="form-control input-sm" placeholder={this.props.namePlaceHolder} value={this.state.name}
                  onChange={(e) => {
                    this.updateState({name: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
            {this.renderChildren()}
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!this.state.show}>
            <button id="wizard_add_button" type="button" className="btn btn-primary" disabled={0 === this.state.name.length}
              onClick={(e) => {
                this.props.onWizardNew(this.state.folder.projectId, `${this.state.folder.data.path}/${this.state.folder.title}`, this.state.name);
                this.hide();
              }}
            >Add</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id="wizard_close_button" type="button" className="btn btn-default" disabled={!this.state.show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}
