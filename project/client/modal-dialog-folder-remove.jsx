
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFolderRemove extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.helperModalPopover = new HelperModalPopover();
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  hide() {
    this.props.onHide();
  }
  
  _keyDown(e) {
    if(e.ctrlKey && e.shiftKey && 'R' === e.key) {
      e.preventDefault();
      this._remove();
      this.hide();
    }
    else if(e.ctrlKey && e.shiftKey && 'D' === e.key) {
      e.preventDefault();
      this._delete();
      this.hide();
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="folder_remove_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }

  renderText() {
    if(null !== this.props.folder) {
      return `Do you really want to delete or remove: '${this.props.folder.data.path}/${this.props.folder.title}'?`;
    }
    else {
      return '';
    }
  }
  
  render() {
    const modalResult = this.props.modalResults.get(this.props.name);
    const show = modalResult?.visible;
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_folder_remove" aria-labelledby="contained-modal-title-sm" show={show}
        onHide={(e) => {
          this.hide();
        }}
      >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          {this.renderText()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+R" style={{display:'inline-block'}} disabled={!show}>
            <button id="folder_remove_remove_button" type="button" className="btn btn-warning"
              onClick={(e) => {
                this.props.onFolderRemove(this.props.folder.projectId, this.props.folder.title, this.props.folder.key, this.props.folder.data);
              }}
            >Remove</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+D" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="folder_remove_delete_button" type="button" className="btn btn-danger"
              onClick={(e) => {
                this.props.onFolderDelete(this.props.folder.projectId, this.props.folder.title, this.props.folder.key, this.props.folder.data);
              }}
            >Delete</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="folder_remove_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>  
        </ModalFooter>
      </Modal>
    );
  }
}
