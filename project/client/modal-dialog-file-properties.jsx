
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import InputAnalyzer from './helper/input-analyzer';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogFileProperties extends ReactComponentBase {
  constructor(props) {
    super(props, {
      fileName: '',
      valid: false,
      error: false,
      errorText: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.inputAnalyzer = new InputAnalyzer(this.props.capitalFirst ? true : false);
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.props.modalResults, nextProps.modalResults)
      || !this.shallowCompare(this.state, nextState);
  }
  
  didUpdate(prevProps, prevState) {
    const previousModalResult = prevProps.modalResults.get(this.props.name);
    const modalResult = this.props.modalResults.get(this.props.name);
    if(modalResult) {
      if(!prevProps.modalResult || (prevProps.modalResult.visible !== modalResult.visible)) {
        if(modalResult.visible) {
          this.eventListener.init();
        }
        else {
          this.eventListener.exit();
        }
      }
    }
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  update() {
    this.props.onFileProperties(this.props.file.projectId, this.props.file.path, this.props.file.title, `${this.state.fileName}.${this._getFileExtension(this.props.file)}`);
  }
  
  hide() {
    this.updateState({
      fileName: {$set: ''},
      valid: {$set: false},
      error: {$set: false},
      errorText: {$set: ''}
    });
    this.props.onHide();
  }
    
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.update();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
    const modalResult = this.props.modalResults.get(this.props.name);
    if(this.state.error || 'error' === modalResult?.result.code) {
      const msg = this.state.error ? this.state.errorText : modalResult.result.msg;
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 420
      };
      const labelStyle = {
        top: '2px',
        position: 'relative'
      };
      return (
        <div style={errorDivStyle}>
          <p id="file_properties_modal_error_label" className="text-danger control-label modal_body_p_as_label" style={labelStyle}>{msg}</p>
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  render() {
    const modalResult = this.props.modalResults.get(this.props.name);
    const show = !!modalResult?.visible;
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_file_properties" aria-labelledby="contained-modal-title-sm" show={show}
        onShown={(e) => {
          this.inputRef.current.select();
        }}
        onHide={(e) => {
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-2">
                <label htmlFor="modal_dialog_file_properties_name_input" className="control-label">Name</label>
              </div>
              <div className="col-sm-9">
                <input ref={this.inputRef} id="modal_dialog_file_properties_name_input" type="text" className="form-control input-sm" placeholder="file name" value={this.state.fileName ? this.state.fileName : this._getFileName(this.props.file)}
                  onChange={(e) => {
                    const result = this.inputAnalyzer.analyze(e.target.value);
                    if(result.success) {
                      this.updateState({valid: {$set: true}});
                      this.updateState({error: {$set: false}});
                    }
                    else {
                      this.updateState({valid: {$set: false}});
                      this.updateState({error: {$set: true}});
                      if(!result.regexpValidation) {
                        this.updateState({errorText: {$set: `The name contains not allowed characters: '${result.noneValidCharacters}'`}});
                      }
                      else if(!result.filenameValidation) {
                        this.updateState({errorText: {$set: `The filename is not allowed: '${result.noneValidFilename}'`}});
                      }
                    }
                    this.updateState({fileName: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-2">
                <p className="control-label modal_body_p_as_label">File name</p>
              </div>
              <div className="col-sm-9" style={{textAlign: 'left'}}>
                <p className="control-label modal_body_p_as_label">{`${this.state.fileName ? this.state.fileName : this._getFileName(this.props.file)}.${this._getFileExtension(this.props.file)}`}</p>
              </div>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!show}>
            <button id="file_properties_change_button" type="button" className="btn btn-primary" disabled={(0 === this.state.fileName.length || this._getFileName(this.props.file) === this.state.fileName || !this.state.valid)}
              onClick={(e) => {
                this.update();
              }}
              >Change</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!show}>
            <button id="file_properties_close_button" type="button" className="btn btn-default" disabled={!show}
              onClick={(e) => {
                this.hide();
              }}
              >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
  
  _getFileName(file) {
    if(file) {
      const index = file.title.indexOf('.');
      return file.title.substring(0, index);
    }
    else {
      return 'NONE';
    }
  }
  
  _getFileExtension(file) {
    if(file) {
      const index = file.title.indexOf('.');
      return file.title.substring(index + 1);
    }
    else {
      return 'NONE';
    }
  }
}
