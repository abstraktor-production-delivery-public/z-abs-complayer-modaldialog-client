
'use strict';

import HelperEventListener from './helper/helper-event-listener';
import HelperModalPopover from './helper/helper-modal-popover';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ModalDialogSearch extends ReactComponentBase {
  constructor(props) {
    super(props, {
      show: false,
      search: ''
    });
    this.helperModalPopover = new HelperModalPopover();
    this.inputRef = React.createRef();
    this.eventListener = new HelperEventListener('keydown', this._keyDown.bind(this), true);
  }
  
  didMount() {
    this.helperModalPopover.didMount();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.result, nextProps.result)
      || !this.shallowCompare(this.state, nextState);
  }
  
  willUnmount() {
    this.eventListener.exit();
  }
  
  show() {
    this.updateState({
      show: {$set: true},
      search: {$set: ''}
    });
    this.eventListener.init();
  }
  
  hide() {
    this.eventListener.exit();
    this.updateState({
      show: {$set: false}
    });
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && !e.shiftKey && 'Enter' === e.key) {
      if(!this.state.error) {
        e.preventDefault();
        this.props.onSearch(this.state.search);
        this.hide();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      e.preventDefault();
      this.hide();
    }
  }
  
  renderErrorMessage() {
    /*if(this.props.result.code !== 'success') {
      let errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        width: 440
      }
      return (
        <div style={errorDivStyle}>
          <p className="text-danger control-label modal_body_p_as_label">{this.props.result.msg}</p>
        </div>
      );
    }*/
  }

  render() {
    let nameDivLabelClassName = 'col-sm-10';
    let errorShow = false;
    if(undefined !== this.props.result.status && !this.props.result.status.name.status) {
      nameDivLabelClassName += ' has-error';
      errorShow = true;
    }
    this.helperModalPopover.onRender();
    return (
      <Modal ref={this.helperModalPopover.modalRef} id="modal_search" aria-labelledby="contained-modal-title-sm" show={this.state.show || errorShow}
        onShown={(e) => {
          this.inputRef.current.select();
        }}
        onHide={(e) => {
          this.hide();
        }}
        >
        <ModalHeader closeButton>
          <img className="modal_header_icon pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon"></img>
          <h4 className="modal_header_heading modal-sm">Search</h4>
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <div className="row" style={{textAlign: 'right'}}>
              <div className="col-sm-2">
                <label htmlFor="modal_dialog_search_find_what" className="control-label">Name</label>
              </div>
              <div className="col-sm-9">
                <input ref={this.inputRef} type="text" id="modal_dialog_search_find_what" className="form-control input-sm" placeholder="find what" value={this.state.search}
                  onChange={(e) => {
                    this.updateState({search: {$set: e.target.value}});
                  }}
                  />
              </div>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Enter" style={{display:'inline-block'}} disabled={!this.state.show}>
            <button id="modal_dialog_search_button" type="button" className="btn btn-primary" disabled={0 === this.state.search.length}
              onClick={(e) => {
                this.props.onSearch(this.state.search);
                this.hide();
              }}
            >Search</button>
          </Popover>
          <Popover ref={this.helperModalPopover.getPopoverRef()} placement="bottom" heading="" content="" shortcut="Ctrl+Shift+C" style={{display:'inline-block',marginLeft:'5px'}} disabled={!this.state.show}>
            <button id="modal_dialog_close_button" type="button" className="btn btn-default" disabled={!this.state.show}
              onClick={(e) => {
                this.hide();
              }}
            >Close</button>
          </Popover>
        </ModalFooter>
      </Modal>
    );
  }
}

